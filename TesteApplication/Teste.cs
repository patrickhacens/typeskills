﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TesteApplication
{
    public partial class Teste : Form
    {
        public Teste()
        {
            InitializeComponent();
        }

        string sText = "Puta que o pariu, meu gato pois um ovo, mas gato nao poe ovo, puta que o pariu denovo preciso de mais texto para que de 3 linhas inteiras no texto geral ali no formulario";
        RectangleF[] Rectangles;
        private void Teste_Load(object sender, EventArgs e)
        {
            Calculate();
        }

        private void Calculate()
        {
            Letters.TextRenderer renderer = new Letters.TextRenderer(this.CreateGraphics(), this.Font);

            StringFormat f = new StringFormat();
            f.LineAlignment = StringAlignment.Far;
            f.Alignment = StringAlignment.Center;

            Rectangles = renderer.MeasureEachCharacter(sText, this.ClientRectangle, f);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (Rectangles != null)
            {
                StringFormat f = new StringFormat();
                f.Alignment = StringAlignment.Center;
                f.LineAlignment = StringAlignment.Center;
                Graphics graph = this.CreateGraphics();
                for (int i = 0; i < sText.Length; i++)
                {
                    graph.DrawString(sText[i].ToString(), this.Font, Brushes.Black, Rectangles[i],f);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Calculate();
            this.Invalidate();
            this.Update();
        }
    }
}
