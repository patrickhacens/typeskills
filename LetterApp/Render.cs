﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Letters;
using System.Threading;

namespace TypeSkills
{
	public partial class Render : UserControl
	{
		#region Properties
		[Browsable(false)]
		public Letters.LetterCollection Letters { get; set; }

		[Category("Configurações")]
		public Color NotTypedColor { get; set; }

		[Category("Configurações")]
		public Color IncorrectColor { get; set; }

		[Category("Configurações")]
		public Color CorrectColor { get; set; }
		
		#endregion Properties

		StringFormat format = new StringFormat();

		#region Events
		public delegate void EndOfLettersDelegate(object sender, LetterCollection Letters);
		public delegate void TypedLetterDelegate(object sender, Letter Letter);
		public delegate void DeletedLetterDelegate(object sender, Letter Letter);

		/// <summary>
		/// Its raised when is typed the last letter of the collection
		/// </summary>
		public event EndOfLettersDelegate EndOfLetters;
		/// <summary>
		/// Its raised when is correct typed a letter of the collection
		/// </summary>
		public event TypedLetterDelegate CorrectTyped;
		/// <summary>
		/// Its raised when is incorrect typed a letter of the collection
		/// </summary>
		public event TypedLetterDelegate IncorrectTyped;
		/// <summary>
		/// Its raised when is backspaced a letter of the collection
		/// </summary>
		public event DeletedLetterDelegate DeletedLetter;
		/// <summary>
		/// Its Raised when the first letter is typed in the collection
		/// </summary>
		public event TypedLetterDelegate FirstLetterTyped;
		#endregion Events

		public Render()
		{
			
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);

			format.FormatFlags = StringFormatFlags.NoClip;
			format.Alignment = StringAlignment.Center;
			format.LineAlignment = StringAlignment.Near;
			format.Trimming = StringTrimming.None;

			InitializeComponent();
			Letters = new Letters.LetterCollection();
			this.Paint += Form1_Paint;
		}

		#region Private methods
		private void Form1_Paint(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			DrawLetters(e.Graphics);
		}

		private void DrawLetters(Graphics graph)
		{
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            string ToDraw = Letters.ToString();
            Letters.TextRenderer render = new Letters.TextRenderer(graph, this.Font);
            var rectangles = render.MeasureEachCharacter(ToDraw, new RectangleF(this.Padding.Left,this.Padding.Top,this.Width - this.Padding.Horizontal,this.Height - this.Padding.Vertical), format);
            int n = 0;
            int c = Letters.Count(d => d.Status != LetterStatus.NotTyped);

            foreach (RectangleF rec in rectangles)
            {
                if (c == n)
                {
                    //graph.FillRectangle(Brushes.SteelBlue, rec);
                    graph.DrawLine(new Pen(Brushes.SteelBlue, 5), new PointF(rec.X, rec.Y + rec.Height), new PointF(rec.X + rec.Width, rec.Y + rec.Height));
                }
                graph.DrawString(ToDraw[n].ToString(), this.Font, GetBrushes(Letters.Letters[n]), rec, format);
                if (char.IsWhiteSpace(ToDraw[n]))
                {
                    graph.DrawLine(new Pen(GetBrushes(Letters.Letters[n]), 2), new PointF(rec.X + rec.Width / 3, rec.Y + rec.Height * 0.9f), new PointF(rec.X + rec.Width - rec.Width / 3, rec.Y + rec.Height * 0.9f));
                }
                n++;
            }
		}

		private Brush GetBrushes(Letter l)
		{
			switch (l.Status)
			{
				case LetterStatus.NotTyped:
					return new SolidBrush(NotTypedColor);
				case LetterStatus.Incorrect:
					return new SolidBrush(IncorrectColor);
				case LetterStatus.Correct:
					return new SolidBrush(CorrectColor);
				default:
					return Brushes.Black;
			}
		}

		private void Form1_SizeChanged(object sender, EventArgs e)
		{
			this.Invalidate();
		}

		private void CheckRaiseEndOFLetters()
		{
			if (this.Letters.All(d => d.Status != LetterStatus.NotTyped))
			{
				if (EndOfLetters != null)
					EndOfLetters(this, Letters);
			}
		}
		#endregion private methods

		/// <summary>
		/// Clears the last typed character
		/// </summary>
		public void BackSpace()
		{
			Letter l = this.Letters.LastOrDefault(d=>d.Status != LetterStatus.NotTyped);
			if (l == null)
				return;

			l.Status = LetterStatus.NotTyped;
			if (DeletedLetter != null)
				DeletedLetter(this, l);
			this.Invalidate();
		}

		/// <summary>
		/// Types a character into the collection
		/// </summary>
		/// <param name="character">character typed</param>
		public void InsertKey(char character)
		{
			Letter l = this.Letters.FirstOrDefault(d => d.Status == LetterStatus.NotTyped);
			if (l == null)
				return;

			if (this.Letters.Letters[0].Status == LetterStatus.NotTyped)
			{
				if (FirstLetterTyped != null)
					FirstLetterTyped(this, l);
			}

			if (l.Character == character)
			{
				l.Status = LetterStatus.Correct;
				if (CorrectTyped != null)
					CorrectTyped(this, l);
			}
			else
			{
				if (IncorrectTyped != null)
					IncorrectTyped(this, l);
				l.Status = LetterStatus.Incorrect;
			}
			this.Invalidate();

			CheckRaiseEndOFLetters();
		}

		/// <summary>
		/// Changes the current set of letters in the collection
		/// </summary>
		/// <param name="Frase"></param>
		public void NewPhrase(string Frase)
		{
			this.Letters.Clear();
			this.Letters.AddRange(Frase);
			this.Invalidate();
		}
	}
}
