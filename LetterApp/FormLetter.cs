﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using TypeSkills.WordFactory;

namespace TypeSkills
{
	public partial class FormLetter : Base
	{
		Timer smallClock = new Timer();
		DateTime? Start;
		DateTime? End;
        DataBase db = new DataBase();

		private ICharFactory factory { get; set; }
		private ICharFactory Factory
		{
			get
			{
				return factory;
			}
			set
			{
				factory = value;
				UpdateLeveis();
			}
		}

		public FormLetter()
		{
			InitializeComponent();
			#region DataBindings
			this.Render.DataBindings.Add(new System.Windows.Forms.Binding("Font", this.settingsBindingSource, "Fonte", true));
			this.Render.DataBindings.Add(new System.Windows.Forms.Binding("CorrectColor", this.settingsBindingSource, "CorAcerto", true));
			this.Render.DataBindings.Add(new System.Windows.Forms.Binding("IncorrectColor", this.settingsBindingSource, "CorErro", true));
			settingsBindingSource.DataSource = global::TypeSkills.Properties.Settings.Default;
			#endregion DataBindings
			AtualizarModo();
		}

		private void UpdateLeveis()
		{
			int level = global::TypeSkills.Properties.Settings.Default.Level;
			LevelToolStrip.DropDownItems.Clear();
			foreach (Level lvl in Factory.GetLeveis())
			{
				ToolStripButton item = new ToolStripButton();
				item.Text = lvl.Display;
				item.Tag = lvl.Valor;
				item.Click += ChangeLevel;
				LevelToolStrip.DropDownItems.Add(item);
			}

			Level l = Factory.GetLeveis().FirstOrDefault(d => d.Valor == level);
			if (l != null)
			{
				ChangeLevel(LevelToolStrip.DropDownItems.OfType<ToolStripItem>().FirstOrDefault(d=>Convert.ToInt32(d.Tag) == l.Valor),new EventArgs());
			}
			else
			{
				ChangeLevel(LevelToolStrip.DropDownItems.OfType<ToolStripItem>().First(), new EventArgs());
			}
		}

		private void ChangeLevel(object sender, EventArgs e)
		{
			ToolStripItem item = sender as ToolStripItem;
			if (item == null)
				return;

			Factory.ChangeLevel(Convert.ToInt32(item.Tag));
			LevelToolStrip.Text = item.Text;
			global::TypeSkills.Properties.Settings.Default.Level = Convert.ToInt32(item.Tag);
			global::TypeSkills.Properties.Settings.Default.Save();
			Render.NewPhrase(Factory.GetNewSetOfWords());
		}

		private void Form2_Load(object sender, EventArgs e)
		{
			Render.Letters = new Letters.LetterCollection();
			Render.NewPhrase(factory.GetNewSetOfWords());
            //teclado.Location = new Point((this.splitContainer1.Panel1.Width - teclado.Width) / 2, this.splitContainer1.Panel1.Height - teclado.Height);
			this.splitContainer1.Panel1.SizeChanged += Panel1_SizeChanged;
		}

		void Panel1_SizeChanged(object sender, EventArgs e)
		{
            //teclado.Location = new Point((this.splitContainer1.Panel1.Width - teclado.Width) / 2, this.splitContainer1.Panel1.Height - teclado.Height);
		}

		private void AtualizarModo()
		{
			Modo modo = (Modo)global::TypeSkills.Properties.Settings.Default.Modo;
			Factory = CharactersFactory.GetNewWordSet(modo);
			modoToolStripMenuItem.Text = "Modo " + modo.ToString();
		}

		#region KeyEvents
		private void FormLetter_KeyPressAtRender(object sender, KeyPressEventArgs e)
		{
			Render.InsertKey(e.KeyChar);
		}

		private void FormLetter_KeyDownAtRender(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					e.SuppressKeyPress = true;
					break;
				case Keys.Back:
					Render.BackSpace();
					e.SuppressKeyPress = true;
					break;
				case Keys.Escape:
					OpenInfos(false);
					e.SuppressKeyPress = true;
					break;
				default:
					break;
			}
		}

		private void FormLetter_KeyDownAtInfo(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					CloseInfos();
					break;
				default:
					break;
			}
		}
		#endregion Keyevents

		#region Render Events
		private void Render_CorrectTyped(object sender, Letters.Letter Letter)
		{
			using (SoundPlayer p = new SoundPlayer())
			{
				p.Stream = global::TypeSkills.Properties.Resources.typeSound;
				p.Play();
			}
            db.Digito.AddDigitoRow(Letter.Character, true, DateTime.Now);
		}

		private void Render_IncorrectTyped(object sender, Letters.Letter Letter)
		{
			using (SoundPlayer p = new SoundPlayer())
			{
				p.Stream = global::TypeSkills.Properties.Resources.erroSound;
				p.Play();
			}
            db.Digito.AddDigitoRow(Letter.Character, false, DateTime.Now);
		}

		private void Render_EndOfLetters(object sender, Letters.LetterCollection Letters)
		{
			End = DateTime.Now;
            long LettersCount = Render.Letters.Count(d => d.Status == global::Letters.LetterStatus.Correct);
            db.Precisao.AddPrecisaoRow(Render.Letters.Count(), LettersCount, DateTime.Now);
            db.Velocidade.AddVelocidadeRow(LettersCount, (End - Start).Value, DateTime.Now);
			OpenInfos(true);
            db.Save();
		}
		private void Render_FirstLetterTyped(object sender, Letters.Letter Letter)
		{
			Start = DateTime.Now;
			Render.FirstLetterTyped -= Render_FirstLetterTyped;
		}

		#endregion Render Events

		private void OpenInfos(bool Finished)
		{
			this.KeyDown -= FormLetter_KeyDownAtRender;
			this.KeyPress -= FormLetter_KeyPressAtRender;
			Render.FirstLetterTyped += Render_FirstLetterTyped;
			if (Finished)
			{
				lblPrecisao.Text = (GetPrecisao() * 100).ToString("0") + " %";
				lblWps.Text = GetWPM().ToString("0.00") + "WPM";
			}
			else
			{
				lblWps.Text = "";
				lblPrecisao.Text = "";
			}
			smallClock.Interval = 30;
			smallClock.Tick += smallClock_TickToOpen;
			smallClock.Start();
		}

		private void CloseInfos()
		{
			this.KeyDown -= FormLetter_KeyDownAtInfo;
			
			lblWps.Text = string.Empty;
			smallClock.Tick += smallClock_TickToClose;
			smallClock.Start();
		}

		private double GetWPM()
		{
			double miliseconds = (End - Start).Value.TotalMilliseconds;
			double words = Render.Letters.Letters.Length / 5d;
			double minutes = (miliseconds / 1000 / 60);
			return words / minutes;
		}

		private float GetPrecisao()
		{
			return (float)Render.Letters.Count(l=>l.Status == Letters.LetterStatus.Correct) / (float)Render.Letters.Letters.Length;
		}

		private void smallClock_TickToOpen(object sender, EventArgs e)
		{
			if (splitContainer1.SplitterDistance > this.splitContainer1.Width *0.7)
			{
				splitContainer1.SplitterDistance -= (int)(this.splitContainer1.Width * 0.7 / 10);
			}
			else
			{
				smallClock.Tick -= smallClock_TickToOpen;
				smallClock.Stop();
				this.KeyDown += FormLetter_KeyDownAtInfo;
			}
		}

		private void smallClock_TickToClose(object sender, EventArgs e)
		{
			if (splitContainer1.SplitterDistance < this.splitContainer1.Width - 1 )
			{
				splitContainer1.SplitterDistance += (int)(this.splitContainer1.Width * 0.7 / 10);
			}
			else
			{
				smallClock.Tick -= smallClock_TickToClose;
				
				smallClock.Stop();
				this.Render.NewPhrase(Factory.GetNewSetOfWords());
				this.KeyPress += FormLetter_KeyPressAtRender;
				this.KeyDown += FormLetter_KeyDownAtRender;
			}
		}
		#region Toolstrip
		#region Configurações Toolstrip
		private void corDeAcertoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				global::TypeSkills.Properties.Settings.Default.CorAcerto = colorDialog.Color;
				global::TypeSkills.Properties.Settings.Default.Save();
			}
		}

		private void corDeErroToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				global::TypeSkills.Properties.Settings.Default.CorErro = colorDialog.Color;
				global::TypeSkills.Properties.Settings.Default.Save();
			}
		}

		private void fonteToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (fontDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				global::TypeSkills.Properties.Settings.Default.Fonte = fontDialog.Font;
				global::TypeSkills.Properties.Settings.Default.Save();
			}
		}
		#endregion

		#region Modo Toolstrip
		private void letrasToolStripMenuItem_Click(object sender, EventArgs e)
		{
			global::TypeSkills.Properties.Settings.Default.Modo = 0;
			this.modoToolStripMenuItem.Text = "Modo Letras";
			AtualizarModo();
		}

		private void palavrasToolStripMenuItem_Click(object sender, EventArgs e)
		{
			global::TypeSkills.Properties.Settings.Default.Modo = 1;
			this.modoToolStripMenuItem.Text = "Modo Palavras";
			AtualizarModo();
		}

		private void frasesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			global::TypeSkills.Properties.Settings.Default.Modo = 2;
			this.modoToolStripMenuItem.Text = "Modo Frases";
			AtualizarModo();
		}
		#endregion Modo Toolstrip

        private void velocidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Subforms.FormVelocidade f = new Subforms.FormVelocidade(db);
            f.TopLevel = false;
            f.Parent = this;
            f.BringToFront();
            f.Dock = DockStyle.Fill;
            f.Show();
        }
		#endregion Toolstrip

        private void precisãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Subforms.FormPrecisao f = new Subforms.FormPrecisao(db);
            f.TopLevel = false;
            f.Parent = this;
            f.BringToFront();
            f.Dock = DockStyle.Fill;
            f.Show();
        }
	}
}
