﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TypeSkills.Properties;

namespace TypeSkills
{
    public class DataBase
    {
        public Db db = new Db();
        string dbPath = Application.StartupPath + Resources.DbPath;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs;

        public Db.PrecisaoDataTable Precisao
        {
            get
            {
                return db.Precisao;
            }
        }

        public Db.VelocidadeDataTable Velocidade
        {
            get
            {
                return db.Velocidade;
            }
        }

        public Db.DigitoDataTable Digito
        {
            get
            {
                return db.Digito;
            }
        }

        public DataBase()
        {
            db.RemotingFormat = System.Data.SerializationFormat.Binary;
            if (!File.Exists(dbPath))
            {
                File.Create(dbPath).Dispose();
                Save();
            }
            Load();
        }

        public void Dispose()
        {
            fs.Dispose();
        }

        public void Save()
        {
            fs = new FileStream(dbPath, FileMode.Open, FileAccess.ReadWrite);
            db.AcceptChanges();
            bf.Serialize(fs, db);
            fs.Close();
        }

        public void Reset()
        {
            db.RejectChanges();
        }

        public void Load()
        {
            fs = new FileStream(dbPath, FileMode.Open, FileAccess.ReadWrite);
            object x = bf.Deserialize(fs);
            db = x as Db;
            db.RemotingFormat = System.Data.SerializationFormat.Binary;
            fs.Close();
        }
    }
}
