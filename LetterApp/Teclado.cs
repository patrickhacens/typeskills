﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TypeSkills
{
	public partial class Teclado : UserControl
	{
		public Teclado()
		{
			InitializeComponent();
		}

        public IEnumerable<Button> GetTeclas()
        {
            List<Button> Buttons = new List<Button>();
            foreach (Control item in this.Controls)
            {
                VerifyControl(Buttons, item);
            }
            return Buttons;
        }

        private static void VerifyControl(List<Button> Buttons, Control item)
        {
            if (item is Button)
            {
                Buttons.Add(item as Button);
            }
            if (item.Controls.Count > 0)
            {
                foreach (Control Item in item.Controls)
                {
                    VerifyControl(Buttons, Item);
                }
            }
        }
	}
}
