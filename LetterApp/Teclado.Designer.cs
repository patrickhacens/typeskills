﻿namespace TypeSkills
{
	partial class Teclado
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.keyQuote = new System.Windows.Forms.Button();
            this.key1 = new System.Windows.Forms.Button();
            this.key2 = new System.Windows.Forms.Button();
            this.key3 = new System.Windows.Forms.Button();
            this.key4 = new System.Windows.Forms.Button();
            this.key5 = new System.Windows.Forms.Button();
            this.key6 = new System.Windows.Forms.Button();
            this.key7 = new System.Windows.Forms.Button();
            this.key8 = new System.Windows.Forms.Button();
            this.key9 = new System.Windows.Forms.Button();
            this.key0 = new System.Windows.Forms.Button();
            this.keyHifen = new System.Windows.Forms.Button();
            this.keyEquals = new System.Windows.Forms.Button();
            this.keyBackSpace = new System.Windows.Forms.Button();
            this.keySqrBrcts = new System.Windows.Forms.Button();
            this.keyAccent = new System.Windows.Forms.Button();
            this.keyP = new System.Windows.Forms.Button();
            this.keyO = new System.Windows.Forms.Button();
            this.keyI = new System.Windows.Forms.Button();
            this.keyU = new System.Windows.Forms.Button();
            this.keyY = new System.Windows.Forms.Button();
            this.keyT = new System.Windows.Forms.Button();
            this.keyR = new System.Windows.Forms.Button();
            this.keyE = new System.Windows.Forms.Button();
            this.keyW = new System.Windows.Forms.Button();
            this.keyQ = new System.Windows.Forms.Button();
            this.keyCloseBrackts = new System.Windows.Forms.Button();
            this.keyTil = new System.Windows.Forms.Button();
            this.keyCedilha = new System.Windows.Forms.Button();
            this.keyL = new System.Windows.Forms.Button();
            this.keyK = new System.Windows.Forms.Button();
            this.keyJ = new System.Windows.Forms.Button();
            this.keyH = new System.Windows.Forms.Button();
            this.keyG = new System.Windows.Forms.Button();
            this.k = new System.Windows.Forms.Button();
            this.keyD = new System.Windows.Forms.Button();
            this.keyS = new System.Windows.Forms.Button();
            this.keyA = new System.Windows.Forms.Button();
            this.keyCaps = new System.Windows.Forms.Button();
            this.keyRightShift = new System.Windows.Forms.Button();
            this.keySlash = new System.Windows.Forms.Button();
            this.keyDotComa = new System.Windows.Forms.Button();
            this.keyDot = new System.Windows.Forms.Button();
            this.keyComa = new System.Windows.Forms.Button();
            this.keyM = new System.Windows.Forms.Button();
            this.keyN = new System.Windows.Forms.Button();
            this.keyB = new System.Windows.Forms.Button();
            this.keyV = new System.Windows.Forms.Button();
            this.keyC = new System.Windows.Forms.Button();
            this.keyX = new System.Windows.Forms.Button();
            this.keyZ = new System.Windows.Forms.Button();
            this.keyLeftShift = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.keyTab = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.keyPipe = new System.Windows.Forms.Button();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.keyCtrl = new System.Windows.Forms.Button();
            this.keyWindows = new System.Windows.Forms.Button();
            this.keyAlt = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.keyAltGr = new System.Windows.Forms.Button();
            this.keyRightWindows = new System.Windows.Forms.Button();
            this.keyContextMenu = new System.Windows.Forms.Button();
            this.keyRightCtrl = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // keyQuote
            // 
            this.keyQuote.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyQuote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyQuote.Location = new System.Drawing.Point(3, 3);
            this.keyQuote.Name = "keyQuote";
            this.keyQuote.Size = new System.Drawing.Size(30, 30);
            this.keyQuote.TabIndex = 0;
            this.keyQuote.Text = "\'";
            this.keyQuote.UseVisualStyleBackColor = true;
            // 
            // key1
            // 
            this.key1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key1.Location = new System.Drawing.Point(39, 3);
            this.key1.Name = "key1";
            this.key1.Size = new System.Drawing.Size(30, 30);
            this.key1.TabIndex = 1;
            this.key1.Text = "1";
            this.key1.UseVisualStyleBackColor = true;
            // 
            // key2
            // 
            this.key2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key2.Location = new System.Drawing.Point(75, 3);
            this.key2.Name = "key2";
            this.key2.Size = new System.Drawing.Size(30, 30);
            this.key2.TabIndex = 2;
            this.key2.Text = "2";
            this.key2.UseVisualStyleBackColor = true;
            // 
            // key3
            // 
            this.key3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key3.Location = new System.Drawing.Point(111, 3);
            this.key3.Name = "key3";
            this.key3.Size = new System.Drawing.Size(30, 30);
            this.key3.TabIndex = 3;
            this.key3.Text = "3";
            this.key3.UseVisualStyleBackColor = true;
            // 
            // key4
            // 
            this.key4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key4.Location = new System.Drawing.Point(147, 3);
            this.key4.Name = "key4";
            this.key4.Size = new System.Drawing.Size(30, 30);
            this.key4.TabIndex = 4;
            this.key4.Text = "4";
            this.key4.UseVisualStyleBackColor = true;
            // 
            // key5
            // 
            this.key5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key5.Location = new System.Drawing.Point(183, 3);
            this.key5.Name = "key5";
            this.key5.Size = new System.Drawing.Size(30, 30);
            this.key5.TabIndex = 5;
            this.key5.Text = "5";
            this.key5.UseVisualStyleBackColor = true;
            // 
            // key6
            // 
            this.key6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key6.Location = new System.Drawing.Point(219, 3);
            this.key6.Name = "key6";
            this.key6.Size = new System.Drawing.Size(30, 30);
            this.key6.TabIndex = 6;
            this.key6.Text = "6";
            this.key6.UseVisualStyleBackColor = true;
            // 
            // key7
            // 
            this.key7.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key7.Location = new System.Drawing.Point(255, 3);
            this.key7.Name = "key7";
            this.key7.Size = new System.Drawing.Size(30, 30);
            this.key7.TabIndex = 7;
            this.key7.Text = "7";
            this.key7.UseVisualStyleBackColor = true;
            // 
            // key8
            // 
            this.key8.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key8.Location = new System.Drawing.Point(291, 3);
            this.key8.Name = "key8";
            this.key8.Size = new System.Drawing.Size(30, 30);
            this.key8.TabIndex = 8;
            this.key8.Text = "8";
            this.key8.UseVisualStyleBackColor = true;
            // 
            // key9
            // 
            this.key9.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key9.Location = new System.Drawing.Point(327, 3);
            this.key9.Name = "key9";
            this.key9.Size = new System.Drawing.Size(30, 30);
            this.key9.TabIndex = 9;
            this.key9.Text = "9";
            this.key9.UseVisualStyleBackColor = true;
            // 
            // key0
            // 
            this.key0.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.key0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.key0.Location = new System.Drawing.Point(363, 3);
            this.key0.Name = "key0";
            this.key0.Size = new System.Drawing.Size(30, 30);
            this.key0.TabIndex = 10;
            this.key0.Text = "0";
            this.key0.UseVisualStyleBackColor = true;
            // 
            // keyHifen
            // 
            this.keyHifen.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyHifen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyHifen.Location = new System.Drawing.Point(399, 3);
            this.keyHifen.Name = "keyHifen";
            this.keyHifen.Size = new System.Drawing.Size(30, 30);
            this.keyHifen.TabIndex = 11;
            this.keyHifen.Text = "-";
            this.keyHifen.UseVisualStyleBackColor = true;
            // 
            // keyEquals
            // 
            this.keyEquals.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyEquals.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyEquals.Location = new System.Drawing.Point(435, 3);
            this.keyEquals.Name = "keyEquals";
            this.keyEquals.Size = new System.Drawing.Size(30, 30);
            this.keyEquals.TabIndex = 12;
            this.keyEquals.Text = "=";
            this.keyEquals.UseVisualStyleBackColor = true;
            // 
            // keyBackSpace
            // 
            this.keyBackSpace.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyBackSpace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyBackSpace.Location = new System.Drawing.Point(471, 3);
            this.keyBackSpace.Name = "keyBackSpace";
            this.keyBackSpace.Size = new System.Drawing.Size(84, 30);
            this.keyBackSpace.TabIndex = 13;
            this.keyBackSpace.Text = "<=";
            this.keyBackSpace.UseVisualStyleBackColor = true;
            // 
            // keySqrBrcts
            // 
            this.keySqrBrcts.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keySqrBrcts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keySqrBrcts.Location = new System.Drawing.Point(455, 3);
            this.keySqrBrcts.Name = "keySqrBrcts";
            this.keySqrBrcts.Size = new System.Drawing.Size(30, 30);
            this.keySqrBrcts.TabIndex = 12;
            this.keySqrBrcts.Text = "[";
            this.keySqrBrcts.UseVisualStyleBackColor = true;
            // 
            // keyAccent
            // 
            this.keyAccent.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyAccent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyAccent.Location = new System.Drawing.Point(419, 3);
            this.keyAccent.Name = "keyAccent";
            this.keyAccent.Size = new System.Drawing.Size(30, 30);
            this.keyAccent.TabIndex = 11;
            this.keyAccent.Text = "´";
            this.keyAccent.UseVisualStyleBackColor = true;
            // 
            // keyP
            // 
            this.keyP.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyP.Location = new System.Drawing.Point(383, 3);
            this.keyP.Name = "keyP";
            this.keyP.Size = new System.Drawing.Size(30, 30);
            this.keyP.TabIndex = 10;
            this.keyP.Text = "P";
            this.keyP.UseVisualStyleBackColor = true;
            // 
            // keyO
            // 
            this.keyO.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyO.Location = new System.Drawing.Point(347, 3);
            this.keyO.Name = "keyO";
            this.keyO.Size = new System.Drawing.Size(30, 30);
            this.keyO.TabIndex = 9;
            this.keyO.Text = "O";
            this.keyO.UseVisualStyleBackColor = true;
            // 
            // keyI
            // 
            this.keyI.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyI.Location = new System.Drawing.Point(311, 3);
            this.keyI.Name = "keyI";
            this.keyI.Size = new System.Drawing.Size(30, 30);
            this.keyI.TabIndex = 8;
            this.keyI.Text = "I";
            this.keyI.UseVisualStyleBackColor = true;
            // 
            // keyU
            // 
            this.keyU.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyU.Location = new System.Drawing.Point(275, 3);
            this.keyU.Name = "keyU";
            this.keyU.Size = new System.Drawing.Size(30, 30);
            this.keyU.TabIndex = 7;
            this.keyU.Text = "U";
            this.keyU.UseVisualStyleBackColor = true;
            // 
            // keyY
            // 
            this.keyY.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyY.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyY.Location = new System.Drawing.Point(239, 3);
            this.keyY.Name = "keyY";
            this.keyY.Size = new System.Drawing.Size(30, 30);
            this.keyY.TabIndex = 6;
            this.keyY.Text = "Y";
            this.keyY.UseVisualStyleBackColor = true;
            // 
            // keyT
            // 
            this.keyT.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyT.Location = new System.Drawing.Point(203, 3);
            this.keyT.Name = "keyT";
            this.keyT.Size = new System.Drawing.Size(30, 30);
            this.keyT.TabIndex = 5;
            this.keyT.Text = "T";
            this.keyT.UseVisualStyleBackColor = true;
            // 
            // keyR
            // 
            this.keyR.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyR.Location = new System.Drawing.Point(167, 3);
            this.keyR.Name = "keyR";
            this.keyR.Size = new System.Drawing.Size(30, 30);
            this.keyR.TabIndex = 4;
            this.keyR.Text = "R";
            this.keyR.UseVisualStyleBackColor = true;
            // 
            // keyE
            // 
            this.keyE.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyE.Location = new System.Drawing.Point(131, 3);
            this.keyE.Name = "keyE";
            this.keyE.Size = new System.Drawing.Size(30, 30);
            this.keyE.TabIndex = 3;
            this.keyE.Text = "E";
            this.keyE.UseVisualStyleBackColor = true;
            // 
            // keyW
            // 
            this.keyW.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyW.Location = new System.Drawing.Point(95, 3);
            this.keyW.Name = "keyW";
            this.keyW.Size = new System.Drawing.Size(30, 30);
            this.keyW.TabIndex = 2;
            this.keyW.Text = "W";
            this.keyW.UseVisualStyleBackColor = true;
            // 
            // keyQ
            // 
            this.keyQ.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyQ.Location = new System.Drawing.Point(59, 3);
            this.keyQ.Name = "keyQ";
            this.keyQ.Size = new System.Drawing.Size(30, 30);
            this.keyQ.TabIndex = 1;
            this.keyQ.Text = "Q";
            this.keyQ.UseVisualStyleBackColor = true;
            // 
            // keyCloseBrackts
            // 
            this.keyCloseBrackts.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyCloseBrackts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyCloseBrackts.Location = new System.Drawing.Point(471, 3);
            this.keyCloseBrackts.Name = "keyCloseBrackts";
            this.keyCloseBrackts.Size = new System.Drawing.Size(30, 30);
            this.keyCloseBrackts.TabIndex = 12;
            this.keyCloseBrackts.Text = "]";
            this.keyCloseBrackts.UseVisualStyleBackColor = true;
            // 
            // keyTil
            // 
            this.keyTil.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyTil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyTil.Location = new System.Drawing.Point(435, 3);
            this.keyTil.Name = "keyTil";
            this.keyTil.Size = new System.Drawing.Size(30, 30);
            this.keyTil.TabIndex = 11;
            this.keyTil.Text = "~";
            this.keyTil.UseVisualStyleBackColor = true;
            // 
            // keyCedilha
            // 
            this.keyCedilha.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyCedilha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyCedilha.Location = new System.Drawing.Point(399, 3);
            this.keyCedilha.Name = "keyCedilha";
            this.keyCedilha.Size = new System.Drawing.Size(30, 30);
            this.keyCedilha.TabIndex = 10;
            this.keyCedilha.Text = "Ç";
            this.keyCedilha.UseVisualStyleBackColor = true;
            // 
            // keyL
            // 
            this.keyL.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyL.Location = new System.Drawing.Point(363, 3);
            this.keyL.Name = "keyL";
            this.keyL.Size = new System.Drawing.Size(30, 30);
            this.keyL.TabIndex = 9;
            this.keyL.Text = "L";
            this.keyL.UseVisualStyleBackColor = true;
            // 
            // keyK
            // 
            this.keyK.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyK.Location = new System.Drawing.Point(327, 3);
            this.keyK.Name = "keyK";
            this.keyK.Size = new System.Drawing.Size(30, 30);
            this.keyK.TabIndex = 8;
            this.keyK.Text = "K";
            this.keyK.UseVisualStyleBackColor = true;
            // 
            // keyJ
            // 
            this.keyJ.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyJ.Location = new System.Drawing.Point(291, 3);
            this.keyJ.Name = "keyJ";
            this.keyJ.Size = new System.Drawing.Size(30, 30);
            this.keyJ.TabIndex = 7;
            this.keyJ.Text = "J";
            this.keyJ.UseVisualStyleBackColor = true;
            // 
            // keyH
            // 
            this.keyH.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyH.Location = new System.Drawing.Point(255, 3);
            this.keyH.Name = "keyH";
            this.keyH.Size = new System.Drawing.Size(30, 30);
            this.keyH.TabIndex = 6;
            this.keyH.Text = "H";
            this.keyH.UseVisualStyleBackColor = true;
            // 
            // keyG
            // 
            this.keyG.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyG.Location = new System.Drawing.Point(219, 3);
            this.keyG.Name = "keyG";
            this.keyG.Size = new System.Drawing.Size(30, 30);
            this.keyG.TabIndex = 5;
            this.keyG.Text = "G";
            this.keyG.UseVisualStyleBackColor = true;
            // 
            // k
            // 
            this.k.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.k.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.k.Location = new System.Drawing.Point(183, 3);
            this.k.Name = "k";
            this.k.Size = new System.Drawing.Size(30, 30);
            this.k.TabIndex = 4;
            this.k.Text = "F";
            this.k.UseVisualStyleBackColor = true;
            // 
            // keyD
            // 
            this.keyD.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyD.Location = new System.Drawing.Point(147, 3);
            this.keyD.Name = "keyD";
            this.keyD.Size = new System.Drawing.Size(30, 30);
            this.keyD.TabIndex = 3;
            this.keyD.Text = "D";
            this.keyD.UseVisualStyleBackColor = true;
            // 
            // keyS
            // 
            this.keyS.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyS.Location = new System.Drawing.Point(111, 3);
            this.keyS.Name = "keyS";
            this.keyS.Size = new System.Drawing.Size(30, 30);
            this.keyS.TabIndex = 2;
            this.keyS.Text = "S";
            this.keyS.UseVisualStyleBackColor = true;
            // 
            // keyA
            // 
            this.keyA.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyA.Location = new System.Drawing.Point(75, 3);
            this.keyA.Name = "keyA";
            this.keyA.Size = new System.Drawing.Size(30, 30);
            this.keyA.TabIndex = 1;
            this.keyA.Text = "A";
            this.keyA.UseVisualStyleBackColor = true;
            // 
            // keyCaps
            // 
            this.keyCaps.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyCaps.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyCaps.Location = new System.Drawing.Point(3, 3);
            this.keyCaps.Name = "keyCaps";
            this.keyCaps.Size = new System.Drawing.Size(66, 30);
            this.keyCaps.TabIndex = 0;
            this.keyCaps.Text = "caps";
            this.keyCaps.UseVisualStyleBackColor = true;
            // 
            // keyRightShift
            // 
            this.keyRightShift.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyRightShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyRightShift.Location = new System.Drawing.Point(491, 3);
            this.keyRightShift.Name = "keyRightShift";
            this.keyRightShift.Size = new System.Drawing.Size(66, 30);
            this.keyRightShift.TabIndex = 13;
            this.keyRightShift.Text = "shift^";
            this.keyRightShift.UseVisualStyleBackColor = true;
            // 
            // keySlash
            // 
            this.keySlash.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keySlash.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keySlash.Location = new System.Drawing.Point(455, 3);
            this.keySlash.Name = "keySlash";
            this.keySlash.Size = new System.Drawing.Size(30, 30);
            this.keySlash.TabIndex = 12;
            this.keySlash.Text = "/";
            this.keySlash.UseVisualStyleBackColor = true;
            // 
            // keyDotComa
            // 
            this.keyDotComa.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyDotComa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyDotComa.Location = new System.Drawing.Point(419, 3);
            this.keyDotComa.Name = "keyDotComa";
            this.keyDotComa.Size = new System.Drawing.Size(30, 30);
            this.keyDotComa.TabIndex = 11;
            this.keyDotComa.Text = ";";
            this.keyDotComa.UseVisualStyleBackColor = true;
            // 
            // keyDot
            // 
            this.keyDot.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyDot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyDot.Location = new System.Drawing.Point(383, 3);
            this.keyDot.Name = "keyDot";
            this.keyDot.Size = new System.Drawing.Size(30, 30);
            this.keyDot.TabIndex = 10;
            this.keyDot.Text = ".";
            this.keyDot.UseVisualStyleBackColor = true;
            // 
            // keyComa
            // 
            this.keyComa.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyComa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyComa.Location = new System.Drawing.Point(347, 3);
            this.keyComa.Name = "keyComa";
            this.keyComa.Size = new System.Drawing.Size(30, 30);
            this.keyComa.TabIndex = 9;
            this.keyComa.Text = ",";
            this.keyComa.UseVisualStyleBackColor = true;
            // 
            // keyM
            // 
            this.keyM.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyM.Location = new System.Drawing.Point(311, 3);
            this.keyM.Name = "keyM";
            this.keyM.Size = new System.Drawing.Size(30, 30);
            this.keyM.TabIndex = 8;
            this.keyM.Text = "M";
            this.keyM.UseVisualStyleBackColor = true;
            // 
            // keyN
            // 
            this.keyN.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyN.Location = new System.Drawing.Point(275, 3);
            this.keyN.Name = "keyN";
            this.keyN.Size = new System.Drawing.Size(30, 30);
            this.keyN.TabIndex = 7;
            this.keyN.Text = "N";
            this.keyN.UseVisualStyleBackColor = true;
            // 
            // keyB
            // 
            this.keyB.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyB.Location = new System.Drawing.Point(239, 3);
            this.keyB.Name = "keyB";
            this.keyB.Size = new System.Drawing.Size(30, 30);
            this.keyB.TabIndex = 6;
            this.keyB.Text = "B";
            this.keyB.UseVisualStyleBackColor = true;
            // 
            // keyV
            // 
            this.keyV.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyV.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyV.Location = new System.Drawing.Point(203, 3);
            this.keyV.Name = "keyV";
            this.keyV.Size = new System.Drawing.Size(30, 30);
            this.keyV.TabIndex = 5;
            this.keyV.Text = "V";
            this.keyV.UseVisualStyleBackColor = true;
            // 
            // keyC
            // 
            this.keyC.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyC.Location = new System.Drawing.Point(167, 3);
            this.keyC.Name = "keyC";
            this.keyC.Size = new System.Drawing.Size(30, 30);
            this.keyC.TabIndex = 4;
            this.keyC.Text = "C";
            this.keyC.UseVisualStyleBackColor = true;
            // 
            // keyX
            // 
            this.keyX.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyX.Location = new System.Drawing.Point(131, 3);
            this.keyX.Name = "keyX";
            this.keyX.Size = new System.Drawing.Size(30, 30);
            this.keyX.TabIndex = 3;
            this.keyX.Text = "X";
            this.keyX.UseVisualStyleBackColor = true;
            // 
            // keyZ
            // 
            this.keyZ.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyZ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyZ.Location = new System.Drawing.Point(95, 3);
            this.keyZ.Name = "keyZ";
            this.keyZ.Size = new System.Drawing.Size(30, 30);
            this.keyZ.TabIndex = 2;
            this.keyZ.Text = "Z";
            this.keyZ.UseVisualStyleBackColor = true;
            // 
            // keyLeftShift
            // 
            this.keyLeftShift.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyLeftShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyLeftShift.Location = new System.Drawing.Point(3, 3);
            this.keyLeftShift.Name = "keyLeftShift";
            this.keyLeftShift.Size = new System.Drawing.Size(50, 30);
            this.keyLeftShift.TabIndex = 0;
            this.keyLeftShift.Text = "^";
            this.keyLeftShift.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.keyQuote);
            this.flowLayoutPanel1.Controls.Add(this.key1);
            this.flowLayoutPanel1.Controls.Add(this.key2);
            this.flowLayoutPanel1.Controls.Add(this.key3);
            this.flowLayoutPanel1.Controls.Add(this.key4);
            this.flowLayoutPanel1.Controls.Add(this.key5);
            this.flowLayoutPanel1.Controls.Add(this.key6);
            this.flowLayoutPanel1.Controls.Add(this.key7);
            this.flowLayoutPanel1.Controls.Add(this.key8);
            this.flowLayoutPanel1.Controls.Add(this.key9);
            this.flowLayoutPanel1.Controls.Add(this.key0);
            this.flowLayoutPanel1.Controls.Add(this.keyHifen);
            this.flowLayoutPanel1.Controls.Add(this.keyEquals);
            this.flowLayoutPanel1.Controls.Add(this.keyBackSpace);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(562, 39);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.keyTab);
            this.flowLayoutPanel2.Controls.Add(this.keyQ);
            this.flowLayoutPanel2.Controls.Add(this.keyW);
            this.flowLayoutPanel2.Controls.Add(this.keyE);
            this.flowLayoutPanel2.Controls.Add(this.keyR);
            this.flowLayoutPanel2.Controls.Add(this.keyT);
            this.flowLayoutPanel2.Controls.Add(this.keyY);
            this.flowLayoutPanel2.Controls.Add(this.keyU);
            this.flowLayoutPanel2.Controls.Add(this.keyI);
            this.flowLayoutPanel2.Controls.Add(this.keyO);
            this.flowLayoutPanel2.Controls.Add(this.keyP);
            this.flowLayoutPanel2.Controls.Add(this.keyAccent);
            this.flowLayoutPanel2.Controls.Add(this.keySqrBrcts);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(6, 44);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(562, 39);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // keyTab
            // 
            this.keyTab.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyTab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyTab.Location = new System.Drawing.Point(3, 3);
            this.keyTab.Name = "keyTab";
            this.keyTab.Size = new System.Drawing.Size(50, 30);
            this.keyTab.TabIndex = 0;
            this.keyTab.Text = "<>";
            this.keyTab.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.keyCaps);
            this.flowLayoutPanel3.Controls.Add(this.keyA);
            this.flowLayoutPanel3.Controls.Add(this.keyS);
            this.flowLayoutPanel3.Controls.Add(this.keyD);
            this.flowLayoutPanel3.Controls.Add(this.k);
            this.flowLayoutPanel3.Controls.Add(this.keyG);
            this.flowLayoutPanel3.Controls.Add(this.keyH);
            this.flowLayoutPanel3.Controls.Add(this.keyJ);
            this.flowLayoutPanel3.Controls.Add(this.keyK);
            this.flowLayoutPanel3.Controls.Add(this.keyL);
            this.flowLayoutPanel3.Controls.Add(this.keyCedilha);
            this.flowLayoutPanel3.Controls.Add(this.keyTil);
            this.flowLayoutPanel3.Controls.Add(this.keyCloseBrackts);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(6, 85);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(562, 39);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.keyLeftShift);
            this.flowLayoutPanel4.Controls.Add(this.keyPipe);
            this.flowLayoutPanel4.Controls.Add(this.keyZ);
            this.flowLayoutPanel4.Controls.Add(this.keyX);
            this.flowLayoutPanel4.Controls.Add(this.keyC);
            this.flowLayoutPanel4.Controls.Add(this.keyV);
            this.flowLayoutPanel4.Controls.Add(this.keyB);
            this.flowLayoutPanel4.Controls.Add(this.keyN);
            this.flowLayoutPanel4.Controls.Add(this.keyM);
            this.flowLayoutPanel4.Controls.Add(this.keyComa);
            this.flowLayoutPanel4.Controls.Add(this.keyDot);
            this.flowLayoutPanel4.Controls.Add(this.keyDotComa);
            this.flowLayoutPanel4.Controls.Add(this.keySlash);
            this.flowLayoutPanel4.Controls.Add(this.keyRightShift);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(6, 126);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(562, 39);
            this.flowLayoutPanel4.TabIndex = 3;
            // 
            // keyPipe
            // 
            this.keyPipe.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyPipe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyPipe.Location = new System.Drawing.Point(59, 3);
            this.keyPipe.Name = "keyPipe";
            this.keyPipe.Size = new System.Drawing.Size(30, 30);
            this.keyPipe.TabIndex = 1;
            this.keyPipe.Text = "\\";
            this.keyPipe.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.keyCtrl);
            this.flowLayoutPanel5.Controls.Add(this.keyWindows);
            this.flowLayoutPanel5.Controls.Add(this.keyAlt);
            this.flowLayoutPanel5.Controls.Add(this.button4);
            this.flowLayoutPanel5.Controls.Add(this.keyAltGr);
            this.flowLayoutPanel5.Controls.Add(this.keyRightWindows);
            this.flowLayoutPanel5.Controls.Add(this.keyContextMenu);
            this.flowLayoutPanel5.Controls.Add(this.keyRightCtrl);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(6, 171);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(562, 39);
            this.flowLayoutPanel5.TabIndex = 4;
            // 
            // keyCtrl
            // 
            this.keyCtrl.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyCtrl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyCtrl.Location = new System.Drawing.Point(3, 3);
            this.keyCtrl.Name = "keyCtrl";
            this.keyCtrl.Size = new System.Drawing.Size(66, 30);
            this.keyCtrl.TabIndex = 0;
            this.keyCtrl.Text = "Ctrl";
            this.keyCtrl.UseVisualStyleBackColor = true;
            // 
            // keyWindows
            // 
            this.keyWindows.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyWindows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyWindows.Location = new System.Drawing.Point(75, 3);
            this.keyWindows.Name = "keyWindows";
            this.keyWindows.Size = new System.Drawing.Size(30, 30);
            this.keyWindows.TabIndex = 1;
            this.keyWindows.Text = "";
            this.keyWindows.UseVisualStyleBackColor = true;
            // 
            // keyAlt
            // 
            this.keyAlt.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyAlt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyAlt.Location = new System.Drawing.Point(111, 3);
            this.keyAlt.Name = "keyAlt";
            this.keyAlt.Size = new System.Drawing.Size(30, 30);
            this.keyAlt.TabIndex = 2;
            this.keyAlt.Text = "Alt";
            this.keyAlt.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(147, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(230, 30);
            this.button4.TabIndex = 3;
            this.button4.Text = "________";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // keyAltGr
            // 
            this.keyAltGr.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyAltGr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyAltGr.Location = new System.Drawing.Point(383, 3);
            this.keyAltGr.Name = "keyAltGr";
            this.keyAltGr.Size = new System.Drawing.Size(30, 30);
            this.keyAltGr.TabIndex = 4;
            this.keyAltGr.Text = "AltGr";
            this.keyAltGr.UseVisualStyleBackColor = true;
            // 
            // keyRightWindows
            // 
            this.keyRightWindows.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyRightWindows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyRightWindows.Location = new System.Drawing.Point(419, 3);
            this.keyRightWindows.Name = "keyRightWindows";
            this.keyRightWindows.Size = new System.Drawing.Size(30, 30);
            this.keyRightWindows.TabIndex = 5;
            this.keyRightWindows.Text = "";
            this.keyRightWindows.UseVisualStyleBackColor = true;
            // 
            // keyContextMenu
            // 
            this.keyContextMenu.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyContextMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyContextMenu.Location = new System.Drawing.Point(455, 3);
            this.keyContextMenu.Name = "keyContextMenu";
            this.keyContextMenu.Size = new System.Drawing.Size(30, 30);
            this.keyContextMenu.TabIndex = 6;
            this.keyContextMenu.Text = "=";
            this.keyContextMenu.UseVisualStyleBackColor = true;
            // 
            // keyRightCtrl
            // 
            this.keyRightCtrl.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.keyRightCtrl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keyRightCtrl.Location = new System.Drawing.Point(491, 3);
            this.keyRightCtrl.Name = "keyRightCtrl";
            this.keyRightCtrl.Size = new System.Drawing.Size(66, 30);
            this.keyRightCtrl.TabIndex = 7;
            this.keyRightCtrl.Text = "Ctrl";
            this.keyRightCtrl.UseVisualStyleBackColor = true;
            // 
            // Teclado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel5);
            this.Controls.Add(this.flowLayoutPanel4);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(571, 214);
            this.Name = "Teclado";
            this.Size = new System.Drawing.Size(571, 214);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

        public System.Windows.Forms.Button keyQuote;
        public System.Windows.Forms.Button key1;
        public System.Windows.Forms.Button key2;
        public System.Windows.Forms.Button key3;
        public System.Windows.Forms.Button key4;
        public System.Windows.Forms.Button key5;
        public System.Windows.Forms.Button key6;
        public System.Windows.Forms.Button key7;
        public System.Windows.Forms.Button key8;
        public System.Windows.Forms.Button key9;
        public System.Windows.Forms.Button key0;
        public System.Windows.Forms.Button keyHifen;
        public System.Windows.Forms.Button keyEquals;
        public System.Windows.Forms.Button keyBackSpace;
        public System.Windows.Forms.Button keySqrBrcts;
        public System.Windows.Forms.Button keyAccent;
        public System.Windows.Forms.Button keyP;
        public System.Windows.Forms.Button keyO;
        public System.Windows.Forms.Button keyI;
        public System.Windows.Forms.Button keyU;
        public System.Windows.Forms.Button keyY;
        public System.Windows.Forms.Button keyT;
        public System.Windows.Forms.Button keyR;
        public System.Windows.Forms.Button keyE;
        public System.Windows.Forms.Button keyW;
        public System.Windows.Forms.Button keyQ;
        public System.Windows.Forms.Button keyCloseBrackts;
        public System.Windows.Forms.Button keyTil;
        public System.Windows.Forms.Button keyCedilha;
        public System.Windows.Forms.Button keyL;
        public System.Windows.Forms.Button keyK;
        public System.Windows.Forms.Button keyJ;
        public System.Windows.Forms.Button keyH;
        public System.Windows.Forms.Button keyG;
        public System.Windows.Forms.Button k;
        public System.Windows.Forms.Button keyD;
        public System.Windows.Forms.Button keyS;
        public System.Windows.Forms.Button keyA;
        public System.Windows.Forms.Button keyCaps;
        public System.Windows.Forms.Button keyRightShift;
        public System.Windows.Forms.Button keySlash;
        public System.Windows.Forms.Button keyDotComa;
        public System.Windows.Forms.Button keyDot;
        public System.Windows.Forms.Button keyComa;
        public System.Windows.Forms.Button keyM;
        public System.Windows.Forms.Button keyN;
        public System.Windows.Forms.Button keyB;
        public System.Windows.Forms.Button keyV;
        public System.Windows.Forms.Button keyC;
        public System.Windows.Forms.Button keyX;
        public System.Windows.Forms.Button keyZ;
        public System.Windows.Forms.Button keyLeftShift;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        public System.Windows.Forms.Button keyTab;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        public System.Windows.Forms.Button keyPipe;
        public System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        public System.Windows.Forms.Button keyCtrl;
        public System.Windows.Forms.Button keyWindows;
        public System.Windows.Forms.Button keyAlt;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Button keyAltGr;
        public System.Windows.Forms.Button keyRightWindows;
        public System.Windows.Forms.Button keyContextMenu;
        public System.Windows.Forms.Button keyRightCtrl;

    }
}
