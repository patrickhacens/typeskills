﻿namespace TypeSkills
{
	partial class FormLetter
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Letters.LetterCollection letterCollection1 = new Letters.LetterCollection();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configuraçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fonteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corDeAcertoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corDeErroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.letrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.palavrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frasesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LevelToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.estatisticasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.velocidadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.precisãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Render = new TypeSkills.Render();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblWps = new System.Windows.Forms.Label();
            this.lblPrecisao = new System.Windows.Forms.Label();
            this.settingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraçõesToolStripMenuItem,
            this.modoToolStripMenuItem,
            this.LevelToolStrip,
            this.estatisticasToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1420, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configuraçõesToolStripMenuItem
            // 
            this.configuraçõesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fonteToolStripMenuItem,
            this.corDeAcertoToolStripMenuItem,
            this.corDeErroToolStripMenuItem});
            this.configuraçõesToolStripMenuItem.Name = "configuraçõesToolStripMenuItem";
            this.configuraçõesToolStripMenuItem.Size = new System.Drawing.Size(116, 24);
            this.configuraçõesToolStripMenuItem.Text = "Configurações";
            // 
            // fonteToolStripMenuItem
            // 
            this.fonteToolStripMenuItem.Name = "fonteToolStripMenuItem";
            this.fonteToolStripMenuItem.Size = new System.Drawing.Size(170, 24);
            this.fonteToolStripMenuItem.Text = "Fonte";
            this.fonteToolStripMenuItem.Click += new System.EventHandler(this.fonteToolStripMenuItem_Click);
            // 
            // corDeAcertoToolStripMenuItem
            // 
            this.corDeAcertoToolStripMenuItem.Name = "corDeAcertoToolStripMenuItem";
            this.corDeAcertoToolStripMenuItem.Size = new System.Drawing.Size(170, 24);
            this.corDeAcertoToolStripMenuItem.Text = "Cor de Acerto";
            this.corDeAcertoToolStripMenuItem.Click += new System.EventHandler(this.corDeAcertoToolStripMenuItem_Click);
            // 
            // corDeErroToolStripMenuItem
            // 
            this.corDeErroToolStripMenuItem.Name = "corDeErroToolStripMenuItem";
            this.corDeErroToolStripMenuItem.Size = new System.Drawing.Size(170, 24);
            this.corDeErroToolStripMenuItem.Text = "Cor de Erro";
            this.corDeErroToolStripMenuItem.Click += new System.EventHandler(this.corDeErroToolStripMenuItem_Click);
            // 
            // modoToolStripMenuItem
            // 
            this.modoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.letrasToolStripMenuItem,
            this.palavrasToolStripMenuItem,
            this.frasesToolStripMenuItem});
            this.modoToolStripMenuItem.Name = "modoToolStripMenuItem";
            this.modoToolStripMenuItem.Size = new System.Drawing.Size(104, 24);
            this.modoToolStripMenuItem.Text = "Modo Letras";
            // 
            // letrasToolStripMenuItem
            // 
            this.letrasToolStripMenuItem.Name = "letrasToolStripMenuItem";
            this.letrasToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.letrasToolStripMenuItem.Text = "Letras";
            this.letrasToolStripMenuItem.Click += new System.EventHandler(this.letrasToolStripMenuItem_Click);
            // 
            // palavrasToolStripMenuItem
            // 
            this.palavrasToolStripMenuItem.Name = "palavrasToolStripMenuItem";
            this.palavrasToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.palavrasToolStripMenuItem.Text = "Palavras";
            this.palavrasToolStripMenuItem.Click += new System.EventHandler(this.palavrasToolStripMenuItem_Click);
            // 
            // frasesToolStripMenuItem
            // 
            this.frasesToolStripMenuItem.Name = "frasesToolStripMenuItem";
            this.frasesToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.frasesToolStripMenuItem.Text = "Frases";
            this.frasesToolStripMenuItem.Click += new System.EventHandler(this.frasesToolStripMenuItem_Click);
            // 
            // LevelToolStrip
            // 
            this.LevelToolStrip.Name = "LevelToolStrip";
            this.LevelToolStrip.Size = new System.Drawing.Size(67, 24);
            this.LevelToolStrip.Text = "Level 1";
            // 
            // estatisticasToolStripMenuItem
            // 
            this.estatisticasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.velocidadeToolStripMenuItem,
            this.precisãoToolStripMenuItem});
            this.estatisticasToolStripMenuItem.Name = "estatisticasToolStripMenuItem";
            this.estatisticasToolStripMenuItem.Size = new System.Drawing.Size(93, 24);
            this.estatisticasToolStripMenuItem.Text = "Estatisticas";
            // 
            // velocidadeToolStripMenuItem
            // 
            this.velocidadeToolStripMenuItem.Name = "velocidadeToolStripMenuItem";
            this.velocidadeToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.velocidadeToolStripMenuItem.Text = "Velocidade";
            this.velocidadeToolStripMenuItem.Click += new System.EventHandler(this.velocidadeToolStripMenuItem_Click);
            // 
            // precisãoToolStripMenuItem
            // 
            this.precisãoToolStripMenuItem.Name = "precisãoToolStripMenuItem";
            this.precisãoToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.precisãoToolStripMenuItem.Text = "Precisão";
            this.precisãoToolStripMenuItem.Click += new System.EventHandler(this.precisãoToolStripMenuItem_Click);
            // 
            // fontDialog
            // 
            this.fontDialog.Color = System.Drawing.SystemColors.ControlText;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Panel1.Controls.Add(this.Render);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(1408, 773);
            this.splitContainer1.SplitterDistance = 1369;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 2;
            // 
            // Render
            // 
            this.Render.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Render.CorrectColor = System.Drawing.Color.Empty;
            this.Render.IncorrectColor = System.Drawing.Color.Empty;
            letterCollection1.Letters = new Letters.Letter[0];
            this.Render.Letters = letterCollection1;
            this.Render.Location = new System.Drawing.Point(10, 16);
            this.Render.Margin = new System.Windows.Forms.Padding(5);
            this.Render.Name = "Render";
            this.Render.NotTypedColor = System.Drawing.Color.Silver;
            this.Render.Padding = new System.Windows.Forms.Padding(300, 30, 30, 30);
            this.Render.Size = new System.Drawing.Size(1350, 748);
            this.Render.TabIndex = 0;
            this.Render.EndOfLetters += new TypeSkills.Render.EndOfLettersDelegate(this.Render_EndOfLetters);
            this.Render.CorrectTyped += new TypeSkills.Render.TypedLetterDelegate(this.Render_CorrectTyped);
            this.Render.IncorrectTyped += new TypeSkills.Render.TypedLetterDelegate(this.Render_IncorrectTyped);
            this.Render.FirstLetterTyped += new TypeSkills.Render.TypedLetterDelegate(this.Render_FirstLetterTyped);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Controls.Add(this.lblWps);
            this.panel1.Controls.Add(this.lblPrecisao);
            this.panel1.Location = new System.Drawing.Point(3, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(55, 755);
            this.panel1.TabIndex = 0;
            // 
            // lblWps
            // 
            this.lblWps.AutoSize = true;
            this.lblWps.Font = new System.Drawing.Font("Lucida Console", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWps.Location = new System.Drawing.Point(13, 68);
            this.lblWps.Name = "lblWps";
            this.lblWps.Size = new System.Drawing.Size(0, 44);
            this.lblWps.TabIndex = 1;
            // 
            // lblPrecisao
            // 
            this.lblPrecisao.AutoSize = true;
            this.lblPrecisao.Font = new System.Drawing.Font("Lucida Console", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecisao.Location = new System.Drawing.Point(13, 7);
            this.lblPrecisao.Name = "lblPrecisao";
            this.lblPrecisao.Size = new System.Drawing.Size(0, 44);
            this.lblPrecisao.TabIndex = 0;
            // 
            // settingsBindingSource
            // 
            this.settingsBindingSource.DataSource = typeof(System.Configuration.ApplicationSettingsBase);
            // 
            // FormLetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1420, 812);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormLetter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TypeSkills";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormLetter_KeyDownAtRender);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormLetter_KeyPressAtRender);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private Render Render;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem configuraçõesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fonteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem corDeAcertoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem corDeErroToolStripMenuItem;
		private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.FontDialog fontDialog;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.ToolStripMenuItem modoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem letrasToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem palavrasToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem frasesToolStripMenuItem;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblPrecisao;
		private System.Windows.Forms.Label lblWps;
		private System.Windows.Forms.ToolStripMenuItem LevelToolStrip;
        private System.Windows.Forms.BindingSource settingsBindingSource;
        private System.Windows.Forms.ToolStripMenuItem estatisticasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem velocidadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem precisãoToolStripMenuItem;
	}
}