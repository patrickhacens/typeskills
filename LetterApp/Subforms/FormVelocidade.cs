﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace TypeSkills.Subforms
{
    public partial class FormVelocidade : TypeSkills.Base
    {
        DataBase db;
        public FormVelocidade(DataBase db)
        {
            InitializeComponent();
            this.db = db;
            AtualizarGrafico();
        }

        private void AtualizarGrafico()
        {
            var dados = db.Velocidade.Select(d => d);
            IEnumerable<IGrouping<int, Db.VelocidadeRow>> agrupamento;
            if (rbSemanal.Checked)
            {
                dados = dados.Where(d=>d.Data >= DateTime.Now.AddDays(-7));
                agrupamento = dados.GroupBy(d => d.Data.Day);
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = DateTime.Now.Day;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = DateTime.Now.AddDays(-7).Day;
            }
            else if (rbMensal.Checked)
            {
                dados = dados.Where(d => d.Data >= DateTime.Now.AddMonths(-1));
                agrupamento = dados.GroupBy(d => d.Data.Day);
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).Day;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = 1;
            }
            else
            {
                dados = dados.Where(d => d.Data >= DateTime.Now.AddYears(-1));
                agrupamento = dados.GroupBy(d => d.Data.Month);
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = 1;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = 12;
            }

            double media = agrupamento.Average(d => d.Average(i => (i.Caracteres / 5) / (i.Tempo.TotalMilliseconds / 1000 / 60)));
            var Dados = chart1.Series["Velocidades"];
            Dados.Points.Clear();
            var Media = chart1.Series["Média"];
            
            Media.Points.Clear();
            foreach (var item in agrupamento)
            {
                DataPoint point = new DataPoint();
                point.SetValueXY(item.Key, item.Average(i => (i.Caracteres / 5) / (i.Tempo.TotalMilliseconds / 1000 / 60)));
                Dados.Points.Add(point);
                DataPoint pointMedia = new DataPoint();
                pointMedia.SetValueXY(item.Key, media);
                Media.Points.Add(pointMedia);
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            AtualizarGrafico();
            chart1.ResetAutoValues();
        }
    }
}
