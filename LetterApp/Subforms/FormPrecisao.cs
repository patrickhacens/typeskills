﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace TypeSkills.Subforms
{
    public partial class FormPrecisao : TypeSkills.Base
    {
        DataBase db;
        public FormPrecisao(DataBase db)
        {
            InitializeComponent();
            this.db = db;
        }

        private void AtualizarGrafico()
        {
            var dados = db.Precisao.Select(d => d);
            IEnumerable<IGrouping<int, Db.PrecisaoRow>> agrupamento;
            if (rbSemanal.Checked)
            {
                dados = dados.Where(d => d.Data >= DateTime.Now.AddDays(-7));
                agrupamento = dados.GroupBy(d => d.Data.Day);
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = DateTime.Now.Day;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = DateTime.Now.AddDays(-7).Day;
            }
            else if (rbMensal.Checked)
            {
                dados = dados.Where(d => d.Data >= DateTime.Now.AddMonths(-1));
                agrupamento = dados.GroupBy(d => d.Data.Day);
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).Day;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = 1;
            }
            else
            {
                dados = dados.Where(d => d.Data >= DateTime.Now.AddYears(-1));
                agrupamento = dados.GroupBy(d => d.Data.Month);
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = 1;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = 12;
            }

            //double media = agrupamento.Average(d => d.Average(i => (i.Caracteres / 5) / (i.Tempo.TotalMilliseconds / 1000 / 60)));
            double media = agrupamento.Average(d => d.Average(i => (i.Acerto / i.Caracteres)*100));
            var Dados = chart1.Series["Precisoes"];
            Dados.Points.Clear();
            var Media = chart1.Series["Media"];

            Media.Points.Clear();
            foreach (var item in agrupamento)
            {
                DataPoint point = new DataPoint();
                point.SetValueXY(item.Key, item.Average(i=>(i.Acerto / i.Caracteres) *100));
                Dados.Points.Add(point);
                DataPoint pointMedia = new DataPoint();
                pointMedia.SetValueXY(item.Key, media);
                Media.Points.Add(pointMedia);
            }
        }

        private void rbSemanal_CheckedChanged(object sender, EventArgs e)
        {
            AtualizarGrafico();
            AtualizarTeclado();
        }

        private void FormPrecisao_Load(object sender, EventArgs e)
        {
            AtualizarGrafico();
            AtualizarTeclado();
        }

        private void AtualizarTeclado()
        {
            Color ErroColor = Color.Red;

            var dados = db.Digito.Select(d => d);
            IEnumerable<IGrouping<int, Db.DigitoRow>> agrupamento;
            if (rbSemanal.Checked)
            {
                dados = dados.Where(d => d.Data >= DateTime.Now.AddDays(-7));
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = DateTime.Now.Day;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = DateTime.Now.AddDays(-7).Day;
            }
            else if (rbMensal.Checked)
            {
                dados = dados.Where(d => d.Data >= DateTime.Now.AddMonths(-1));
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).Day;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = 1;
            }
            else
            {
                dados = dados.Where(d => d.Data >= DateTime.Now.AddYears(-1));
                (chart1.ChartAreas[0] as ChartArea).AxisX.Minimum = 1;
                (chart1.ChartAreas[0] as ChartArea).AxisX.Maximum = 12;
            }

            
            foreach (var button in teclado1.GetTeclas())
	        {
                decimal errados = 0;
                decimal todos = 0;
                decimal wrongPercent = 0;
                if (button.Text.Length == 1)
                {
                    var dadosFiltrados = dados.Where(d => d.Caracter == button.Text.ToLower()[0]);
                    if (dadosFiltrados.Count() > 0)
                    {
                        errados = dadosFiltrados.Count(d => !d.Acerto);
                        if (errados > 0 )
                        {
                        
                        }
                        todos = dadosFiltrados.Count();
                        wrongPercent =  (errados/todos)  * 100;
                    }
                }
                button.BackColor = Color.FromArgb((int)((wrongPercent / 100) * 255), 123, 62);
	        }
            
        }
    }
}
