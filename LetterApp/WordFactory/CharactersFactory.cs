﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeSkills.WordFactory
{
	public static class CharactersFactory
	{
		public static ICharFactory GetNewWordSet(Modo modo)
		{
			switch (modo)
			{
				case Modo.Palavras:
					return new WordFactory();
				case Modo.Frases:
					return new FraseFactory();
				default:
					return new LetterFactory();
			}
		}
	}
}
