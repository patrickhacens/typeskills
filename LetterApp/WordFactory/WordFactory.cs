﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TypeSkills.Properties;
namespace TypeSkills.WordFactory
{
	public class WordFactory : ICharFactory
	{
		private int Level { get; set; }

        private string[] palavras;
        private string palavrasPath = Application.StartupPath + Resources.PalavrasPath;
		private Random random = new Random();

		public WordFactory()
		{
            palavras = System.IO.File.ReadAllLines(palavrasPath, Encoding.Default);
		}

		public string GetNewSetOfWords()
		{
			string[] Palavras;
			int n = GetLengthByLevel();
            Palavras = palavras.Where(d => d.Length > n)
                .OrderBy(d=>Guid.NewGuid())
                .Take(random.Next(5, 10))
                .ToArray();

			StringBuilder sb = new StringBuilder();
			foreach (string word in Palavras)
			{
				if (Level > 5)
				{
					if (random.Next(1, 5) > 3)
					{
						string newWord = word[0].ToString().ToUpper() + word.Substring(1);
						sb.Append(newWord);
					}
					else
					{
						sb.Append(word.ToLower());
					}
					
				}
				else
				{
					sb.Append(RemoveHardCharacters(word.ToLower()));
				}
				
				sb.Append(" ");
			}
			return sb.ToString().Trim();
		}

		private int GetLengthByLevel()
		{
			int n = Level;
			if (Level > 5)
			{
				n = (int)Level - 5;
			}
			if (n == 1)
			{
				return 0;
			}
			else if (n == 5)
			{
				return 20;
			}
			else
			{
				return (int)Math.Pow(2, n);
			}
		}

		private string RemoveHardCharacters(string p)
		{
			return p.Replace('á', 'a').Replace('ã', 'a').Replace('à', 'a').Replace('ä', 'a')
				.Replace('é', 'e').Replace('è', 'e').Replace('ë', 'e')
				.Replace('í', 'i').Replace('í', 'i').Replace('ï', 'i')
				.Replace('ó', 'o').Replace('õ', 'o').Replace('ò', 'o').Replace('ö', 'o')
				.Replace('ú', 'u').Replace('ù', 'u').Replace('ü', 'u');
		}

		public void ChangeLevel(int Level)
		{
			this.Level = Level;
		}

		public Level[] GetLeveis()
		{
			Level[] Leveis = new Level[10];

			for (int i = 0; i < 10; i++)
			{
				Leveis[i] = new Level { Display = "Level " + (i + 1), Valor = i + 1 };
			}
			return Leveis;
		}
	}
}
