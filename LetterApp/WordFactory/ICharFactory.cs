﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeSkills.WordFactory
{
	public interface ICharFactory
	{
		/// <summary>
		/// Returns a group of Words
		/// </summary>
		/// <returns></returns>
		string GetNewSetOfWords();

		/// <summary>
		/// Changes the level of the CharFactory
		/// </summary>
		/// <param name="Level">Level to which it should go</param>
		void ChangeLevel(int Level);

		/// <summary>
		/// Returns all available leveis of the factory
		/// </summary>
		/// <returns>Array of Level</returns>
		Level[] GetLeveis();
	}

	public class Level
	{
		public string Display { get; set; }
		public int Valor { get; set; }
	}
}
