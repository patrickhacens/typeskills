﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TypeSkills.Properties;

namespace TypeSkills.WordFactory
{
	public class FraseFactory : ICharFactory
	{
		private int Level { get; set; }

        private string[] frases;

		private string LastSetOfWords = null;

		public FraseFactory()
		{
			 frases = File.ReadAllLines(Application.StartupPath + Resources.FrasesPath);
		}

		public string GetNewSetOfWords()
		{
            string newSetOfWords = frases.OrderBy(d => Guid.NewGuid())
                .FirstOrDefault();
			if (newSetOfWords == LastSetOfWords)
			{
				return GetNewSetOfWords();
			}else{
				return newSetOfWords;
			}
			
		}

		public void ChangeLevel(int Level)
		{
			if (Level <= GetLeveis().Count() && Level > 0)
			{
				this.Level = Level;
			}
			else
			{
				throw new ArgumentOutOfRangeException("O valor ultrapassou os valores para o level");
			}
		}

		public Level[] GetLeveis()
		{
			Level[] Leveis = new Level[10];
			for (int i = 0; i < Leveis.Length; i++)
			{
				Leveis[i] = new Level { Display = "Level " + (i + 1), Valor = i + 1 };
			}
			return Leveis;
		}
	}
}
