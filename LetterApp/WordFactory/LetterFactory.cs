﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeSkills.WordFactory
{
	public class LetterFactory : ICharFactory
	{
		public int Level { get; set; }

		private Random random = new Random();

		public LetterFactory()
		{
			Level = 1;
		}

		public string GetNewSetOfWords()
		{
			char[] characters;
			switch (Level)
			{
				case 1:
					characters = new char[] { 'j', 'f' };
					break;
				case 2:
					characters = new char[] { 'k', 'd' };
					break;
				case 3:
					characters = new char[] { 'l', 's' };
					break;
				case 4:
					characters = new char[] { 'a', 'ç' };
					break;
				case 5:
					characters = new char[] { 'g', 'h' };
					break;
				case 6:
					characters = MiddleLine();
					break;
				case 7:
					characters = new char[] { 'r', 'u' };
					break;
				case 8:
					characters = new char[] { 'e', 'i' };
					break;
				case 9:
					characters = new char[] { 'w', 'o' };
					break;
				case 10:
					characters = new char[] { 'q', 'p' };
					break;
				case 11:
					characters = new char[] { 't', 'y' };
					break;
				case 12:
					characters = UpperLine();
					break;
				case 13:
					characters = new char[] { 'v', 'm' };
					break;
				case 14:
					characters = new char[] { 'c', ',' };
					break;
				case 15:
					characters = new char[] { 'x', '.' };
					break;
				case 16:
					characters = new char[] { 'z', ';' };
					break;
				case 17:
					characters = LowerLine();
					break;
				case 18:
					characters = new char[] { '4', '7' };
					break;
				case 19:
					characters = new char[] { '3', '8' };
					break;
				case 20:
					characters = new char[] { '2', '9' };
					break;
				case 21:
					characters = new char[] { '1', '0' };
					break;
				case 22:
					characters = NumberLine();
					break;
				case 23:
					characters = LowerLine().Concat(MiddleLine()).Concat(UpperLine()).ToArray();
					break;
				case 24:
					characters = LowerLine().Concat(MiddleLine()).Concat(UpperLine()).Concat(NumberLine()).ToArray();
					break;
				default:
					characters = new char[0];
					break;
			}
			return GenerateWord(characters);
		}

		private char[] NumberLine()
		{
			return new char[] { '4', '7', '3', '8', '2', '9', '1', '0', };
		}

		private char[] LowerLine()
		{
			return new char[] { 'v', 'm', 'c', ',' ,'x', '.' ,'z', ';'};
		}

		private char[] UpperLine()
		{
			return new char[] { 'r', 'u', 'e', 'i', 'w', 'o', 'q', 'p', 't', 'y' };
		}

		private char[] MiddleLine()
		{
			return new char[] { 'j', 'f', 'k', 'd', 'l', 's', 'a', 'ç', 'g', 'h' };
		}

		private string GenerateWord(char[] characteres)
		{
			StringBuilder word = new StringBuilder();
			int quant = random.Next(25, 50);

			bool wasLastSpace = true;

			for (int i = 0; i < quant; i++)
			{
				if (!wasLastSpace && random.Next(0, 100) <= 15)
				{
					word.Append(" "[0]);
					wasLastSpace = true;
				}
				else
				{
					word.Append(characteres[random.Next(0, characteres.Length)]);
					wasLastSpace = false;
				}
			}
			if (char.IsWhiteSpace(word.ToString()[word.Length-1]))
			{
				word.Append(characteres[random.Next(0, characteres.Length)]);
			}
			return word.ToString();
		}

		public void ChangeLevel(int Level)
		{
			this.Level = Level;
		}

		public Level[] GetLeveis()
		{
			Level[] leveis = new Level[24];
			for (int i = 0; i < leveis.Length; i++)
			{
				leveis[i] = new Level { Display = "Level " + (i+1), Valor = i+1 };
			}
			return leveis;
		}
	}
}
