﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Letters
{
	[Browsable(false)]
	public class LetterCollectionEnumerator : IEnumerator<Letter>
	{
		private Letter[] Letters;

		private int position = -1;

		public LetterCollectionEnumerator(Letter[] letters)
		{
			Letters = letters;
		}

		public Letter Current
		{
			get { return Letters[position]; }
		}

		public void Dispose()
		{
			
		}

		object System.Collections.IEnumerator.Current
		{
			get { return Current; }
		}

		public bool MoveNext()
		{
			position = position+1;
			return position < Letters.Length;
		}

		public void Reset()
		{
			position = -1;
		}
	}
}
