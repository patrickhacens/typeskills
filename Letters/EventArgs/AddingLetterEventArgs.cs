﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Letters
{
	public class AddingLetterEventArgs
	{
		public Letter Letter { get; set; }

		public Letter[] Letters { get; set; }

		private bool supress { get; set; }
		public bool Supress
		{
			get
			{ return supress; }
			set
			{
				if (!supress)
				{
					supress = value;
				}
			}
		}
	}
}
