﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Letters
{
	public class LetterAddedEventArgs
	{
		public Letter NewLetter { get; set; }

		public Letter[] Letters { get; set; }
	}
}
