﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Letters
{
	/// <summary>
	/// Defines a letter of the collection
	/// </summary>
	public class Letter
	{
		public char Character { get; set; }

		/// <summary>
		/// Status of the Letter
		/// </summary>
		public LetterStatus Status { get; set; }

		/// <summary>
		/// Creates a new Letter
		/// </summary>
		/// <param name="character">character of the letter</param>
		public Letter(char? character = null)
		{
			if (character.HasValue)
			{
				Character = character.Value;
			}
			Status = LetterStatus.NotTyped;
		}
	}
}
