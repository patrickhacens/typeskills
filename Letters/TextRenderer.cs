﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Letters
{
    public class TextRenderer
    {
        private List<List<RectangleF>> Lines = new List<List<RectangleF>>();

        Font font;
        Graphics graph;

        public TextRenderer(Graphics graph, Font font)
        {
            this.graph = graph;
            this.font = font;
        }


        public RectangleF[] MeasureEachCharacter(string Text, RectangleF Area, StringFormat Format = null)
        {
            if (Format == null)
            {
                Format = new StringFormat();
            }
            List<string> stringLines = new List<string>();
            divideStringNoSpace(ref stringLines, ref Text, Area, Format);

            RectangleF[][] Rectangles = new RectangleF[stringLines.Count][];

            double stringHeight = graph.MeasureString(Text,font,Area.Size).Height;


            int i = 0;
            foreach (String Line in stringLines)
            {
                List<RectangleF> LineRectangles = new List<RectangleF>();

                SizeF stringSize = graph.MeasureString(stringLines[i], this.font);
                int HorizontalPadding = (int)Area.Location.X;
                int VerticalPadding = (int)Area.Location.Y;
                switch (Format.LineAlignment)
                {
                    case StringAlignment.Center:
                        VerticalPadding += (int)((Area.Height - stringHeight) / 2);
                        break;
                    case StringAlignment.Far:
                        VerticalPadding += (int)(Area.Height - stringSize.Height);
                        break;
                }
                if (i > 0)
                {
                    VerticalPadding = (int)Rectangles[i-1].Where(d=>d != null).Max(d => d.Y+ d.Height);
                }

                switch (Format.Alignment)
                {
                    case StringAlignment.Center:
                        HorizontalPadding += (int)((Area.Width - stringSize.Width) / 2);
                        break;
                    case StringAlignment.Far:
                        HorizontalPadding += (int)(Area.Width - stringSize.Width);
                        break;
                }


                

                for (int z = 0; z < Line.Count(); z++)
                {
                    string PreviousLetter = string.Empty;
                    string Letter = string.Empty;
                    string NextLetter = string.Empty;

                    try { PreviousLetter = Line[z - 1].ToString(); }
                    catch (Exception) { }
                    try { Letter = Line[z].ToString(); }
                    catch (Exception) { }
                    try { NextLetter = Line[z + 1].ToString(); }
                    catch (Exception) { }

                    string Subtext = PreviousLetter + Letter + NextLetter;
                    CharacterRange[] CharacterRange = new System.Drawing.CharacterRange[Subtext.Length];
                    for (int x = 0; x < Subtext.Length; x++)
                    {
                        CharacterRange[x] = new CharacterRange { First = x, Length = 1 };
                    }
                    
                    Format.SetMeasurableCharacterRanges(CharacterRange);

                    int znd = 0;
                    if (PreviousLetter != string.Empty)
                    {
                        znd = 1;
                    }
                    var ranges = graph.MeasureCharacterRanges(Subtext, this.font, Area, Format);
                    SizeF LetterSize = ranges[znd].GetBounds(graph).Size;

                    if (LineRectangles.Count == 0)
                    {
                        LineRectangles.Add(
                            new RectangleF
                            {
                                Size = LetterSize,
                                Location = new PointF(HorizontalPadding, Area.Location.Y + VerticalPadding)
                            });
                    }
                    else
                    {
                        RectangleF lastRec = LineRectangles.Last();
                        LineRectangles.Add(
                            new RectangleF
                            {
                                Size = LetterSize,
                                Location = new PointF(lastRec.Location.X + lastRec.Width, lastRec.Y)
                            });
                    }
                }
                Rectangles[i] = LineRectangles.ToArray();
                i++;
            }


            return FlattenArray<RectangleF>(Rectangles);
        }

        private T[] FlattenArray<T>(T[][] MultidimensionalArray)
        {
            List<T> result = new List<T>();

            foreach (var UnidimensionalArray in MultidimensionalArray)
            {
                foreach (var Item in UnidimensionalArray)
                {
                    result.Add(Item);
                }
            }
            return result.ToArray();
        }

        private void divideStringNoSpace(ref List<String> StringList, ref string Text, RectangleF Area, StringFormat Format)
        {
            for (int i = Text.Length; i > 0; i--)
            {
                string testText = Text.Substring(0, i);
                int resultLines;
                int fittedCharacteres;
                SizeF ResultSize = graph.MeasureString(testText, this.font, Area.Size, Format, out fittedCharacteres, out resultLines);

                if (resultLines == 1)
                {
                    StringList.Add(testText);
                    if (i != Text.Length)
                    {
                        string resultText = Text.Substring(i);
                        divideStringNoSpace(ref StringList, ref resultText, Area, Format);
                    }
                    break;
                }
            }

        }
    }
}
