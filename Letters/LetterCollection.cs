﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Letters
{
	public class LetterCollection : IEnumerable<Letter>
	{
		#region Events
		public delegate void AddedLetterHandler(LetterCollection sender, LetterAddedEventArgs e);

		public event AddedLetterHandler AddedLetter;


		public delegate void AddingLetterHandler(LetterCollection sender, AddingLetterEventArgs e);

		public event AddingLetterHandler AddingLetter;
		#endregion

		private Letter[] letters = new Letter[0];

		public Letter[] Letters
		{
			get
			{
				return letters;
			}
			set
			{
				letters = value;
			}
		}

		/// <summary>
		/// Adds a Letter to the collection
		/// </summary>
		/// <param name="letter">Letter to be added to the collection</param>
		public void Add(Letter letter)
		{
			Letter[] nLetters = new Letter[letters.Length + 1];
			letters.CopyTo(nLetters, 0);
			if (AddingLetter != null)
			{
				AddingLetterEventArgs Args = new AddingLetterEventArgs { Letter = letter, Letters = this.Letters, Supress = false };
				AddingLetter(this, Args);
				if(Args.Supress)
				{
					return;
				}
			}
			nLetters[nLetters.Length - 1] = letter;
			if (AddedLetter != null)
				AddedLetter(this, new LetterAddedEventArgs { Letters = this.Letters, NewLetter = letter });
			this.letters = nLetters;
		}

		/// <summary>
		/// Adds a character to the collection
		/// </summary>
		/// <param name="letter">string of which the first character will be added</param>
		public void Add(string letter)
		{
			this.Add(new Letter(letter[0]));
		}

		/// <summary>
		/// Adds a character to the collection
		/// </summary>
		/// <param name="letter">new character</param>
		public void Add(char letter)
		{
			this.Add(new Letter(letter));
		}

		/// <summary>
		/// Adds letters to the collection
		/// </summary>
		/// <param name="letters">Array of Letters to be added</param>
		public void AddRange(Letter[] letters)
		{
			foreach (Letter l in letters)
			{
				this.Add(l);
			}
		}

		/// <summary>
		/// Adds letters to the collection
		/// </summary>
		/// <param name="letters"></param>
		public void AddRange(string letters)
		{
			foreach (char c in letters)
			{
				this.Add(c);
			}
		}

		/// <summary>
		/// Adds letters to the collection
		/// </summary>
		/// <param name="letters"></param>
		public void AddRange(char[] letters)
		{
			foreach (char c in letters)
			{
				this.Add(c);
			}
		}
		
		/// <summary>
		/// Clears the collection
		/// </summary>
		public void Clear()
		{
			this.Letters = new Letter[0];
		}

		public override string ToString()
		{
			return new string(Letters.Select(d => d.Character).ToArray());
		}

		#region IEnumerableComponents
		public IEnumerator<Letter> GetEnumerator()
		{
			return new LetterCollectionEnumerator(Letters);
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
		#endregion
	}
}
